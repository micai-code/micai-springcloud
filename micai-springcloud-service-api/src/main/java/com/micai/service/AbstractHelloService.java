package com.micai.service;

import com.micai.entity.Book;
import org.springframework.web.bind.annotation.*;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/3/13 13:37
 */
@RequestMapping("refactor")
public interface AbstractHelloService {

    @RequestMapping(value = "/hello4", method = RequestMethod.GET)
    String hello4(@RequestParam("name") String name);

    @RequestMapping(value = "/hello5", method = RequestMethod.GET)
    Book hello5(@RequestHeader("name") String name, @RequestHeader("author") String author);

    @RequestMapping(value = "/hello6", method = RequestMethod.POST)
    String hello6(@RequestBody Book book);

}
