package com.micai;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * 描述：
 * <p>
 * Author: 赵新国
 * Date: 2017/12/20 16:24
 */
@EnableEurekaServer // 该注解开启一个服务注册中心提供给其他应用进行对话
@SpringBootApplication
public class EurekaServerApplication {

    public static void main(String [] args) {
        new SpringApplicationBuilder(EurekaServerApplication.class).web(true).run(args);
    }
}
