package com.micai.web;

import com.micai.entity.Book;
import com.micai.service.AbstractHelloService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/3/13 13:45
 */
@RestController
public class RefactorHelloController implements AbstractHelloService {

    @Override
    public String hello4(@RequestParam("name") String name) {
        return "Hello " + name;
    }

    @Override
    public Book hello5(@RequestHeader("name") String name, @RequestHeader("author") String author) {
        return new Book(name, author);
    }

    @Override
    public String hello6(@RequestBody Book book) {
        return "Hello " + book.getName() + ", " + book.getAuthor();
    }
}
