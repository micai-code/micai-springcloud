package com.micai.web;

import com.micai.entity.Book;
import com.micai.entity.BookEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * 描述：
 * <p>
 * Author: 赵新国
 * Date: 2017/12/19 16:03
 */
@RestController
public class HelloWorldController {

    private static final Logger logger = LoggerFactory.getLogger(HelloWorldController.class);

    @Autowired
    private BookEntity bookEntity;

    @Autowired
    private DiscoveryClient client;

    private static  Map<String, Book> map = new HashMap<String, Book>();

    static {
        Book book = new Book();
        book.setId(123423L);
        book.setName("展示");
        book.setDesc("的说法阿斯顿发生的");
        book.setAuthor("沃尔");
        map.put("book", book);
    }

    @GetMapping("/index")
    public String index() {
        ServiceInstance instance = client.getLocalServiceInstance();
        logger.info("/index, host:" + instance.getHost() + ", service_id:" + instance.getServiceId());
        return "index";
    }

    @GetMapping("/hello")
    public String hello() throws Exception {
        ServiceInstance instance = client.getLocalServiceInstance();
        // 模拟一下服务阻塞（长时间未响应）的情况

        // 让处理线程等待几秒钟
        int sleepTime = new Random().nextInt(3000);
        logger.info("sleepTime: {}", sleepTime);
        Thread.sleep(sleepTime);

        logger.info("/hello, host:" + instance.getHost() + ", service_id:" + instance.getServiceId());
        return "Hello World";
    }

    /**
     * Spring Cloud Feign 参数绑定@RequestParam
     * @param name
     * @return
     * @throws Exception
     */
    @GetMapping("/hello1")
    public String hello1(@RequestParam String name) throws Exception {
        ServiceInstance instance = client.getLocalServiceInstance();
        logger.info("/hello1, host:" + instance.getHost() + ", service_id:" + instance.getServiceId());
        return "Hello" + name;
    }

    /**
     * Spring Cloud Feign 参数绑定@RequestHeader
     * @param name
     * @param author
     * @return
     * @throws Exception
     */
    @GetMapping("/hello2")
    public Book hello2(@RequestHeader String name, @RequestHeader String author) throws Exception {
        ServiceInstance instance = client.getLocalServiceInstance();
        logger.info("/hello2, host:" + instance.getHost() + ", service_id:" + instance.getServiceId());
        return new Book(name, author);
    }

    /**
     * Spring Cloud Feign 参数绑定@RequestBody
     * @param book
     * @return
     * @throws Exception
     */
    @PostMapping("/hello3")
    public String hello3(@RequestBody Book book) throws Exception {
        ServiceInstance instance = client.getLocalServiceInstance();
        logger.info("/hello3, host:" + instance.getHost() + ", service_id:" + instance.getServiceId());
        return "Hello " + book.getName() + ", " + book.getAuthor();
    }

    /**
     * 服务提供者-GET方法
     * @return
     */
    @GetMapping("/getBook")
    public Book getBook() {
        ServiceInstance instance = client.getLocalServiceInstance();
        logger.info("/getBook, host:" + instance.getHost() + ", service_id:" + instance.getServiceId());
        return map.get("book");
    }

    /**
     * 服务提供者-POST方法
     * @param book
     * @return
     */
    @PostMapping("/addBook")
    public Book addBook(@RequestBody Book book) {
        ServiceInstance instance = client.getLocalServiceInstance();
        logger.info("/addBook, host:" + instance.getHost() + ", service_id:" + instance.getServiceId() + ", Book: " + book);
        return book;
    }

    /**
     * 服务提供者-PUT方法
     * @param id
     * @param book
     */
    @PutMapping("/putBook/{id}")
    public void putBook(@PathVariable Long id, @RequestBody Book book) {
        ServiceInstance instance = client.getLocalServiceInstance();
        logger.info("/putBook, host:" + instance.getHost() + ", service_id:" + instance.getServiceId() + ", book:" + book);
        Book book1 = map.get("book");
        logger.info("book update start: {}", book1);
        book1.setName(book.getName());
        logger.info("book update end: {}", book1);
    }

    /**
     * 服务提供者-DELETE方法
     * @param id
     */
    @DeleteMapping("/deleteBook/{id}")
    public void deleteBook(@PathVariable Long id) {
        ServiceInstance instance = client.getLocalServiceInstance();
        logger.info("/deleteBook, host:" + instance.getHost() + ", service_id:" + instance.getServiceId() + ", id:" + id);
    }

}
