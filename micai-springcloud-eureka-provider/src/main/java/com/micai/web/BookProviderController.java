package com.micai.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RestController;

/**
 * 描述：图书服务提供者Controller
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/3/1 12:06
 */
@RestController
public class BookProviderController {

    private static final Logger logger = LoggerFactory.getLogger(BookProviderController.class);

    @Autowired
    private DiscoveryClient discoveryClient;


}
