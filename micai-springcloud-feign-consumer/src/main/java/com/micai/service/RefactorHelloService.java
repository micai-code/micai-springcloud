package com.micai.service;

import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/3/13 14:13
 */
@FeignClient(value = "MICAI-SPRINGCLOUD-EUREKA-PROVIDER")
public interface RefactorHelloService extends AbstractHelloService {


}
