package com.micai.service;

import com.micai.entity.Book;
import com.micai.fallback.HelloServiceFallback;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * 描述：通过@FeignClient注解指定服务名来绑定服务，然后在使用SpringMVC的注解来绑定具体该服务提供的REST接口
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/3/13 11:29
 */
/*@FeignClient(value = "MICAI-SPRINGCLOUD-EUREKA-PROVIDER", configuration = DisableHystrixConfigration.class)*/
@FeignClient(value = "micai-springcloud-eureka-provider", fallback = HelloServiceFallback.class)
public interface HelloService {

    @RequestMapping("/hello")
    String hello();

    @RequestMapping("/getBook")
    Book getBook();

    @RequestMapping(value = "/hello1", method = RequestMethod.GET)
    String hello1(@RequestParam("name") String name);

    @RequestMapping(value = "/hello2", method = RequestMethod.GET)
    Book hello2(@RequestHeader("name") String name, @RequestHeader("author") String author);

    @RequestMapping(value = "/hello3", method = RequestMethod.POST)
    String hello3(@RequestBody Book book);

}
