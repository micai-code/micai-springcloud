package com.micai.web;

import com.micai.entity.Book;
import com.micai.service.RefactorHelloService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/3/13 14:16
 */
@RestController
@RequestMapping("/feign-consumer")
public class RefactorConsumerController {

    private static final Logger logger = LoggerFactory.getLogger(RefactorConsumerController.class);

    @Autowired
    RefactorHelloService refactorHelloService;

    @RequestMapping(value = "/hello3", method = RequestMethod.GET)
    public String hello3Consumer() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(refactorHelloService.hello4("MIMI")).append("\n");
        stringBuilder.append(refactorHelloService.hello5("zhaoxinguo", "赵新国")).append("\n");
        stringBuilder.append(refactorHelloService.hello6(new Book("zhaoxinguo", "Java"))).append("\n");
        return stringBuilder.toString();
    }

}
