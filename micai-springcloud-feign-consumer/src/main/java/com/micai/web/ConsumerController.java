package com.micai.web;

import com.micai.entity.Book;
import com.micai.service.HelloService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/3/13 11:37
 */
@RestController
@RequestMapping("/feign-consumer")
public class ConsumerController {

    private static final Logger logger = LoggerFactory.getLogger(ConsumerController.class);

    @Autowired
    HelloService helloService;

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String helloConsumer() {
        String result = helloService.hello();
        return result;
    }

    @RequestMapping(value = "/hello2", method = RequestMethod.GET)
    public String hello2Consumer() {
        StringBuilder stringBuilder = new StringBuilder();
        /*stringBuilder.append(helloService.hello()).append("\n");*/
        stringBuilder.append(helloService.hello1("赵新国")).append("\n");
        stringBuilder.append(helloService.hello2("zhaoxinguo", "赵新国")).append("\n");
        stringBuilder.append(helloService.hello3(new Book("zhaoxinguo", "赵新国"))).append("\n");
        return stringBuilder.toString();
    }

    @RequestMapping(value = "/getBook", method = RequestMethod.GET)
    public Book getBookConsumer() {
        Book book = helloService.getBook();
        logger.info("book: {}", book);
        return book;
    }
}
