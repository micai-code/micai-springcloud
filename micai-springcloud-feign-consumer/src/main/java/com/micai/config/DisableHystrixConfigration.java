/*
package com.micai.config;

import feign.Feign;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

*/
/**
 * 描述：如果不想全局的关闭Hystrix支持，而只想针对某个服务客户端关闭Hystrix支持时，需要通过使用@Scope("prototype")注解为指定的客户端配置Fegin.Builder实例。
 * <p>
 *
 *     关系Hystrix的配置类
 *
 * @author: 赵新国
 * @date: 2018/3/16 9:19
 *//*

@Configuration
public class DisableHystrixConfigration {

    @Bean
    @Scope("prototype")
    public Feign.Builder feignBuilder(){
        return Feign.builder();
    }

}
*/
