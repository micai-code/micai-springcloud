package com.micai;

import feign.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

/**
 * 声明式服务调用模块工程
 */
@EnableFeignClients // 开启 Spring Cloud Feign的支持功能
@EnableDiscoveryClient // 让该应用注册为Eureka客户端应用,以获得服务发现的能力
@SpringBootApplication
public class FeignConsumerApplication {

    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

    public static void main( String[] args ) {
        SpringApplication.run(FeignConsumerApplication.class, args);
    }
}
