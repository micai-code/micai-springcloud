package com.micai.fallback;

import com.micai.entity.Book;
import com.micai.service.HelloService;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 描述：HelloService接口的降级服务
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/3/16 9:32
 */
@Component
public class HelloServiceFallback implements HelloService {

    @Override
    public String hello() {
        return "error";
    }

    @Override
    public Book getBook() {
        return new Book("未知", "error");
    }

    @Override
    public String hello1(@RequestParam("name") String name) {
        return "error";
    }

    @Override
    public Book hello2(@RequestHeader("name") String name, @RequestHeader("author") String author) {
        return new Book("未知", "error");
    }

    @Override
    public String hello3(@RequestBody Book book) {
        return "error";
    }
}
