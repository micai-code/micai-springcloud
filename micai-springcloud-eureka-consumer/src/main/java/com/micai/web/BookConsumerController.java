package com.micai.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * 描述：图书服务消费者Controller
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/3/1 12:07
 */
@RestController
@RequestMapping("/ribbon-consumer")
public class BookConsumerController {

    private static final Logger logger = LoggerFactory.getLogger(ConsumerController.class);

    /**
     * 图书服务提供者URL
     */
    private static final String MICAI_SPRINGCLOUD_EUREKA_PROVIDER_URL = "http://MICAI-SPRINGCLOUD-EUREKA-PROVIDER";

    @Autowired
    private RestTemplate restTemplate;


}
