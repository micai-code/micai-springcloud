package com.micai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * 描述：
 * <p>
 * Author: 赵新国
 * Date: 2017/12/21 17:06
 */
@EnableCircuitBreaker // 开启断路器功能
@EnableDiscoveryClient // 让该应用注册为Eureka客户端应用,以获得服务发现的能力
@SpringBootApplication
public class EurekaConsumerApplication {

    /**
     * 创建RestTemplate的Spring Bean实例,并通过@LoadBalanced注解开启客户端负载均衡
     * @return
     */
    @Bean
    @LoadBalanced
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    public static void main(String [] args) {
        SpringApplication.run(EurekaConsumerApplication.class, args);
    }

}
