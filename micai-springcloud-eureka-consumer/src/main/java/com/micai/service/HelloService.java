package com.micai.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * 描述：
 * <p>
 *
 * @author: 赵新国
 * @date: 2018/3/1 14:27
 */
@Service
public class HelloService {

    private static final Logger logger = LoggerFactory.getLogger(HelloService.class);

    /**
     * 服务提供者URL
     */
    private static final String MICAI_SPRINGCLOUD_EUREKA_PROVIDER_URL = "http://MICAI-SPRINGCLOUD-EUREKA-PROVIDER";

    @Autowired
    RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "helloFallback")
    public String helloService() {
        long start = System.currentTimeMillis();

        ResponseEntity<String> responseEntity = restTemplate.getForEntity(MICAI_SPRINGCLOUD_EUREKA_PROVIDER_URL + "/hello", String.class);
        String body = responseEntity.getBody();

        long end = System.currentTimeMillis();
        logger.info("Spend time : ", (end - start));
        return body;
    }

    public String helloFallback() {
        return "error";
    }

}
