package com.micai.web;

import com.micai.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 描述：
 * <p>
 * Author: 赵新国
 * Date: 2017/12/19 16:03
 */
@RestController
public class HelloWorldController {

    @Autowired
    private Book book;

    @RequestMapping("/hello")
    public String index() {
        return "Hello World";
    }

}
