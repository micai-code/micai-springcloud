package com.micai.entity;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 描述：
 * <p>
 * Author: 赵新国
 * Date: 2017/12/19 16:48
 */
@Component
public class Book {

    @Value("${info.book.name}")
    private String name;

    @Value("${info.book.author}")
    private String author;

    @Value("${info.book.desc}")
    private String desc;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}


