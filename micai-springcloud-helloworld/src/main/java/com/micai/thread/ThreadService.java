package com.micai.thread;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @FileName: ThreadService
 * @Author: zhaoxinguo
 * @Date: 2019/2/11 15:13
 * @Description: 实现CommandLineRunner接口，达到启动应用就开启定时任务线程，任务每间隔一点的时候就执行一次
 */
@Component
public class ThreadService implements CommandLineRunner {

    public static ThreadTask threadTask = null;

    @Override
    public void run(String... strings) throws Exception {
        if (null == threadTask) {
            threadTask = new ThreadTask();
            threadTask.start();
        }
    }
}
