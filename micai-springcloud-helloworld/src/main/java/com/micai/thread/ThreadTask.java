package com.micai.thread;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.channels.InterruptibleChannel;
import java.util.Date;

/**
 * @FileName: ThreadTask
 * @Author: zhaoxinguo
 * @Date: 2019/2/11 15:10
 * @Description: TODO
 */
@Component
public class ThreadTask extends Thread implements InterruptibleChannel {

    private boolean isOpen = true;

    @Override
    public void run() {
        while (isOpen) {
            try {
                System.out.println("当前时间：" + new Date() +"线程启动了。。。。。。");
                Thread.sleep(60000);// 间隔60秒执行一次
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean isOpen() {
        return isOpen;
    }

    @Override
    public void close() throws IOException {
        isOpen = false;
    }
}
