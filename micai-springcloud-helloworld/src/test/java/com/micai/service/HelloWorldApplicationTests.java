package com.micai.service;

import com.micai.HelloWorldApplication;
import com.micai.web.HelloWorldController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * 描述：
 * <p>
 * Author: 赵新国
 * Date: 2017/12/19 16:12
 */
@RunWith(SpringJUnit4ClassRunner.class)
//较新版的Spring Boot取消了@SpringApplicationConfiguration这个注解，用@SpringBootTest就可以了
//@SpringApplicationConfiguration(classes = HelloWorldApplication.class)
@SpringBootTest(classes = HelloWorldApplication.class)
@WebAppConfiguration
public class HelloWorldApplicationTests {

    private MockMvc mvc;

    @Before
    public void setUp() throws Exception {
        mvc = MockMvcBuilders.standaloneSetup(new HelloWorldController()).build();
    }

    @Test
    public void hello() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/hello").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().string(equalTo("Hello World")));
    }

    @Test
    public void math() throws Exception {
        double a = 0.2;
        double b = 0.1;
        double c = 0.8;
        double d = 0.6;
        System.out.println(a-b == b);
        System.out.println(c-d == a);
    }

}
